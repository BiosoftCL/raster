<?php
use Geonodo\Domain\Model\ImportType;
use Illuminate\Database\Seeder;

class ImportTypeTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     * geometric types from: http://postgis.net/docs/using_postgis_dbmanagement.html#Geography_Basics
     *
     * @return void
     */
    public function run()
    {
        ImportType::updateOrCreate([ 'name' => 'Raster' ]);
    }
}