<?php

return [
    'summary' => [
        'ingestiondate' => 'Fecha',
        'instrumentshortname' => 'Fecha',
        'mode' => 'Modo',
        'platformname' => 'Satelite',
        'size' => 'Tamaño',
    ],
    'instrument' => [
        'instrumentshortname' => 'Abreviatura',
        'sensoroperationalmode' => 'Modo',
        'instrumentname' => 'Nombre',
    ],
    'processing' => [
        'onlinequalitycheck' => 'Control de calidad en línea',
        'procfacilityname' => 'Nombre de la instalación de procesamiento',
        'procfacilityorg' => 'Organización de instalaciones de procesamiento',
        'processinglevel' => 'Nivel de procesamiento',
        'processingname' => 'Nombre de procesamiento',
    ],
    'platform' => [
        'sensoroperationalmode' => 'Tipo de Misión',
        'platformidentifier' => 'Identificador NSSDC',
        'procfacilityorg' => 'Operador',
        'platformname' => 'Nombre de satelite',
        'satelliteNumber' => 'Número de satelite',
    ],
    'product' => [
        'instrumentshortname' => 'Acquisition Type',
        'sensoroperationalmode' => 'Byte Order',
        'instrumentname' => 'Cal Compression Type',
        'instrumentname' => 'Circulation flag',
        'instrumentname' => 'Cycle number',
        'instrumentname' => 'Echo Compression Type',
        'gmlfootprint' => 'Footprint',
        'format' => 'Format',
        'ingestiondate' => 'Ingestion Date',
        'footprint' => 'JTS footprint',
        'instrumentname' => 'Mission datatake id',
        'instrumentname' => 'Noise Compression Type',
        'orbitnumber' => 'Orbit number (start)',
        'orbitnumber' => 'Orbit number (stop)',
        'instrumentname' => 'Pass direction',
        'size' => 'Phase identifier',
        'size' => 'Polarisation',
        'size' => 'Product class',
        'size' => 'Product class description',
        'size' => 'Product consolidation',
        'productlevel' => 'Product level',
        'size' => 'Product type',
        'relativeorbitnumber' => 'Relative orbit (start)',
        'relativeorbitnumber' => 'Relative orbit (stop)',
        'size' => 'Sensing start',
        'size' => 'Sensing stop',
        'size' => 'Slice number',
        'size' => 'Status',
        'size' => 'Start relative orbit number',
        'size' => 'Stop relative orbit number',
    ]
];