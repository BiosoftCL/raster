<?php

return [
    'import_layer' => 'Importar raster',
    'form' => [
        'title-import' => 'Importar nueva capa raster',
        'description' => 'Descripción',
        'label-name' => 'Nombre de la capa',
        'message-support-format' => 'Formatos soportados',
        'label-file' => 'Archivo',
    ],
    'menu' => [
        'title' => 'Copernicus',
        'details' => 'Integración con Copernicus'
    ],
    'index' => [
        'title' => 'Copernicus',
        'new_btn' => 'Nuevo',
    ],
];