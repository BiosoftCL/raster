@extends('layouts.app')

@section('content')
    <div class="container-fluid" id="copernicus-app">
        {{-- {{ dd(json_encode($langs))}} --}}
        <copernicus-app :langs="{{ json_encode($langs) }}"/>
        {{--}}<div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render('csw::csw') !!}
            </div>
        </div>{{--}}

        {{-- @component('components.panel.toolbar', ['title' => __('copernicus::common.index.title'), 'flash' => true])
            @slot('toolbar')
                <a href="{{ route('copernicus.index', $instance) }}" class="btn btn-sm btn-default" title="{{__('copernicus::common.index.new_btn')}}">
                    <span>{{ __('copernicus::common.index.new_btn') }}</span>
                </a>
            @endslot
        @endcomponent --}}
    </div>

    <script>
        {!! file_get_contents(COPERNICUS_PATH.'/resources/assets/dist/js/copernicus.js'); !!}
    </script>
@endsection