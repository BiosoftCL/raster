<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label( 'name', __('raster::common.form.label-name'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('spatial_ref_sys') ? 'has-error' : ''}}">
    {!! Form::label('spatial_ref_sys', __('layer.label-option-srdi'), ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <div class="input-group">
            <span class="input-group-addon">{{ __('layer.epsg') }}</span>
            {!! Form::number('spatial_ref_sys', null, ['class'=>'form-control', 'min' => 2000, 'max' => 900913, 'step' => 1]) !!}
        </div>
    </div>
    {!! $errors->first('spatial_ref_sys', '<p class="help-block col-sm-offset-4 col-sm-6">:message</p>') !!}
    <p class="help-block col-sm-offset-4 col-sm-6">{{ __('layer.message-srid')}}</p>
</div>

{{-- <div class="form-group">
    {!! Form::label( 'description' , __('raster::common.form.description'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('description', null, ['class' => 'form-control']) !!}
    </div>
</div> --}}

<div class="form-group {{ $errors->has('layer') ? 'has-error' : ''}}">
    {!! Form::label('layer', __('raster::common.form.label-file'), ['class' => 'col-sm-4 control-label required']) !!}
    <div class="col-sm-6">
        {!! Form::file('layer',['class'=>'form-control']) !!}
        {!! $errors->first('layer', '<p class="help-block">:message</p>') !!}
        <p class="help-block">
            {{ trans('raster::common.form.message-support-format') }}:
            *.tif
        </p>
    </div>
</div>