@extends('layouts.app')

@section('content')
    <div class="container">
        {{-- <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render('layer-services-create') !!}
            </div>
        </div> --}}

        @panel(['title' => __('raster::common.form.title-import')])
            {!! Form::open(['route' => ['vendor_raster.layer-raster.store', $instance], 'class' => 'form-horizontal', 'files' => true]) !!}

            @include('raster::layer-raster._form')

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    {!! Form::submit( __('form.import'), ['class' => 'btn btn-primary']) !!}
                </div>
            </div>

            {!! Form::close() !!}

        @endpanel

    </div>
@endsection

@section('js')
    <script src="/js/layer-service-create.js"></script>
@endsection