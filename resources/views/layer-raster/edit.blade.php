@extends('layouts.app')

@section('content')
    <div class="container">
        {{-- <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render('layer-services-edit') !!}
            </div>
        </div> --}}

        @panel(['title' => __('layer-service.edit_title')])

            {!! Form::model($layerservice, [
                'method' => 'PATCH',
                'route' => ['w.layer-services.update', $instance, $layerservice],
                'class' => 'form-horizontal',
            ]) !!}

            @include ('layer-services._form', ['submitButtonText' => 'Update'])

            {!! Form::close() !!}

        @endpanel

    </div>
@endsection