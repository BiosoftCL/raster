@extends('layouts.app')

@section('content')
    <div class="container" id="app">
        {{-- <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render('layer-services') !!}
            </div>
        </div> --}}

        @panel(['title' => __('layer-service.title-index')])
            @slot('toolbar')
                <a href="{{ route('w.layer-services.create', $instance) }}" class="btn btn-xs btn-default" title="{{ __('form.new') }}">{{ __('form.new') }}</a>
            @endslot

            <div class="table-responsive">
                <table class="table table-borderless" id="datos">
                    <thead>
                    <tr>
                        <th>{{ __('layer.col-name') }}</th>
                        <th>{{ __('layer.col-srs') }}</th>
                        <th>{{ __('layer-service.col-url') }}</th>
                        <th>{{ __('layer-service.col-layer') }}</th>
                        <th class="text-right">
                            <pagination-header :data="{{ $layerServices->toJson() }}"></pagination-header>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($layerServices as $layerService)
                        <tr>
                            <td>{{ $layerService->layer->name }}</td>
                            <td>{{__('layer.epsg')}}{{ $layerService->layer->srid }}</td>
                            <td>{{ $layerService->url }}</td>
                            <td>{{ $layerService->layerTitle }}</td>
                            <td>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'route' => ['w.layer-services.delete', $instance, $layerService],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-xs btn-danger',
                                        'title' => __('layer-service.delete'),
                                        'onclick'=>'return confirm("'.__('layer-service.delete_alert').'")'
                                )) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $layerServices->links() }}
            </div>
        @endpanel

    </div>
@endsection