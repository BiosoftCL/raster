@extends('layouts.app')

@section('content')
    <div class="container">
        {{-- <div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render('layer-services-show') !!}
            </div>
        </div> --}}
        asdasdasdasdasd
        @panel(['title' => __('layer-service.show_title')])
            <a href="{{ route('w.layer-services.edit', [$instance, $layerservice]) }}" class="btn btn-primary btn-sm">
                <span class="glyphicon glyphicon-pencil" aria-hidden="true"/>
            </a>
            {!! Form::open([
                'method' => 'DELETE',
                'route' => ['layerservices', $instance, $layerservice],
                'style' => 'display:inline'
            ]) !!}
            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-sm',
                    'title' => __('layer-service.delete'),
                    'onclick'=>'return confirm("'.__('layer-service.delete_alert').'")'
            ))!!}
            {!! Form::close() !!}
            <div class="table-responsive">
                <table class="table show-table">
                    <thead>
                        <tr>
                            <td class="show-table-label"> {{__('layer-service.col-url')}}</td>
                            <td class="show-table-data"> {{ $layerservice->url }} </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="show-table-label"> {{__('layer-service.col-layer')}}</td>
                            <td class="show-table-data"> {{ $layerservice->layerName }} </td>
                        </tr>
                        <tr>
                            <td class="show-table-label"> {{__('layer-service.col-title')}}</td>
                            <td class="show-table-data"> {{ $layerservice->layerTitle }} </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        @endpanel

    </div>
@endsection