{{-- links de menú para incluir en list-group --}}
{{-- @component('components.menu.item', [
         'route' => route('copernicus.index', $instance),
         'image' => asset('img/menu-icons/csw-external.png'),
         'text' => __('copernicus::common.menu.title'),
         'details' => __('copernicus::common.menu.details')
     ])
@endcomponent --}}

<a href="{{ route('vendor_raster.layer-raster.create', $instance) }}" class="btn btn-xs btn-default"
    title="{{__('raster::common.import_layer')}}">
    <i class="glyphicon glyphicon-open-file"></i>
    <span class="hidden-sm hidden-xs">{{__('raster::common.import_layer')}}</span>
</a>