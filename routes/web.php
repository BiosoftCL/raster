<?php
/*
 * Skeleton routes
 * ------------------
 */
Route::resource('layer-raster', 'RasterController', ['only' => ['index' ,'create', 'store', 'show']]);