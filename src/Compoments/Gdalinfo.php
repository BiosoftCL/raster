<?php
namespace Geonodo\Raster\Compoments;

use Geonodo\Components\Ogrinfo;

/**
 * Class Geonodo\Raster\Compoments\Gdalinfo
 *
 * GDAL Gdalinfo
 *
 * https://gdal.org/programs/gdalinfo.html
 */
class Gdalinfo {

    /**
     * @var array
     */
    private $flags = [];

    /**
     * @var string
     */
    private $layer;

    /**
     * @var string
     */
    private $command = 'gdalinfo';

    /**
     * Gdalinfo constructor.
     *
     * @param string $layer
     */
    public function __construct(string $layer)
    {
        $this->layer = $layer;
    }

    /**
     * Retorna información de una capa
     *
     * @param string $layer
     *
     * @return array
     * @throws \Exception
     */
    static function getLayerInfo($layer)
    {
        $gdalinfo = new self($layer);

        return $gdalinfo->toJson()->execute();
    }

    /**
     * List all metadata domains available for the dataset.
     *
     * @return Gdalinfo
     */
    public function listAllFeatures()
    {
        $this->flags[] = '-listmdd';

        return $this;
    }
    /**
     * List all metadata domains available for the dataset.
     *
     * @return Gdalinfo
     */
    public function toJson()
    {
        $this->flags[] = '-json';

        return $this;
    }

    /**
     * Build command line to execute
     *
     * @return string
     */
    private function build()
    {

        $flags = implode(" ", $this->flags);

        return $this->command." ".$flags." ".$this->layer;
    }

    /**
     * Execute command
     *
     * @return array
     * @throws \Exception
     */
    private function execute()
    {
        $command = $this->build();
        exec($command.' 2>&1', $output);

        if ( ! is_array($output)) {
            throw new \Exception('No command output');
        }
        if ($this->isErrorOutput($output)) {
            logger()->error('Gdalinfo command output fail', $output);
            throw new \Exception('Gdalinfo command output error');
        }

        return $this->parseOutput($output);
    }

    /**
     * Retorna verdadero si la respuesta del comando es algun error
     *
     * @param array $output
     *
     * @return bool
     */
    private function isErrorOutput($output)
    {
        $failAlerts = ['ERROR', 'FAILED', 'FAILURE'];
        foreach ($failAlerts as $failAlert) {
            $res = array_filter($output, function ($value) use ($failAlert) {
                return strpos($value, $failAlert) === 0;
            });
            if ($res) {
                return true;
            }
        }

        return false;
    }

        /**
     * @param array $output
     *
     * @return array
     */
    private function parseOutput($output)
    {
        return json_decode(implode($output));
    }
}