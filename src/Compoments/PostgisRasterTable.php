<?php

namespace Geonodo\Raster\Compoments;

use Exception;
use Geonodo\Components\PostgisTable;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Class PostgisRasterTable
 * Para creación de tabla geometrica
 *
 * @package Geonodo\Components
 */
class PostgisRasterTable extends PostgisTable
{

    /**
     * @var integer
     */
    protected $refSystem = 32719;

    /**
     * @var string
     */
    protected $geometryType = 'polygon';

    /**
     * Geometry column name
     *
     * @var string
     */
    protected $geometryColumn = 'raster';

    /**
     * Return spatial ref system
     *
     * @param string $table PostGis table name
     *
     * @return integer
     * @throw QueryException
     */

    public static function spatialRefSys( $table )
    {
        try {
            return DB::table('raster_columns')
                ->where('r_table_name', '=', $table)
                ->first([ 'srid' ])->srid;
        } catch ( QueryException $e ) {
            throw $e;
        }
    }

    /**
     * PostgisRasterTable constructor.
     *
     * @param string  $tableName
     * @param integer $refSystem  default 32719
     * @param string  $geometryType
     * @param string  $geometryColumn
     * @param integer $dimensions default 2
     */
    public function __construct( $tableName, $refSystem = 32719, $geometryType = '', $geometryColumn = 'geom', $dimensions = 2 )
    {
        parent::__construct($tableName);

        $this->refSystem = $refSystem;
        $this->geometryType = $geometryType;
        $this->geometryColumn = $geometryColumn;
        $this->dimensions = $dimensions;
    }
}
