<?php

namespace Geonodo\Raster\Http\Controllers;

use Doctrine\DBAL\Query\QueryException;
use Geonodo\Raster\Http\Requests\StoreLayer;
use Illuminate\Support\Facades\DB;
use Geonodo\Domain\Model\Instance;
use Geonodo\Raster\Model\LayerRaster;
use Geonodo\Raster\Traits\ImportRaster;
use Geonodo\Raster\Traits\LayerRasterSummary;
use Geonodo\Utils\Postgis\Postgis;
use Illuminate\Routing\Controller;
use Geonodo\Domain\Model\Layer;

class RasterController extends Controller
{
    use LayerRasterSummary;
    use ImportRaster;
    /**
     * Index view
     *
     * @param Instance $instance
     *
     * @return mixed
     */
    public function index(Instance $instance)
    {
        return redirect()->route('w.layer.index', $instance);
        // return view('raster::index', ['langs' => Lang::get('copernicus::attributes')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Instance $instance)
    {
        return view('raster::layer-raster.create', compact('instance'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLayer $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Instance $instance, StoreLayer $request)
    {
        $file = $request->file('layer');
        $path = $file->store('layers');
        $absolutePath = storage_path('app' . DIRECTORY_SEPARATOR . $path);

        //valida el contenido de la capa antes continuar
        if (empty($this->getSummary($absolutePath))) {
            return redirect()->back()->withInput();
        }

        // if ($request->get('mxsig')) {
        //     $request->request->add(['connection' => 'mxsig']);
        // }

        // /** @var integer $spatialRefSys */
        $spatialRefSys = $request->has('spatial_ref_sys') ? $request->input('spatial_ref_sys') : null;
        $layer = $this->importRasterLayer($absolutePath, $request->input('name'), $instance->id, $spatialRefSys, $request->connection);

        if (! $layer) {
            return redirect()->back()->withInput();
        }

        try {
            LayerRaster::create([
                "description"      => $request->input('description'),
                "layer_id" => $layer->id,
            ]);
        } catch (QueryException $e) {
            Postgis::destroyTable($layer->table);
            $layer->delete();
            logger($e->getMessage());

            flash()->error(__('layer-service.message-error-fail'));

            return redirect()->back()->withInput($request->all());
        }

        flash(__('layer.message-success-create', ['number' => DB::table($layer->table)->count()]), 'success')->important();
        return redirect()->route('w.layer.show', [$instance, $layer]);
    }

        /**
     * Display the specified resource.
     *
     * @param Layer $layer
     *
     * @return \Illuminate\View\View
     */
    public function show(Instance $instance, Layer $layer)
    {
        $layer->load(['geometricType', 'importType', 'layerService']);
        return view('raster::layer-raster.show', compact('instance', 'layer'));
    }
}
