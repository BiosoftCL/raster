<?php

namespace Geonodo\Raster\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreLayer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->merge(['file_type' => 'layer']);

        $instance = $this->route('instance_code');

        return [
            // 'geometry'        => 'required|exists:geometric_types,id',
            'layer'           => 'required|file',
            'description'     => 'nullable|max:255',
            'spatial_ref_sys' => 'nullable|numeric|exists:spatial_ref_sys,srid',
            'name'            => [
                'required',
                'max:255',
                Rule::unique('layers')->where(function ($query) use ($instance) {
                    $query->where('instance_id', $instance->id)->where('deleted_at', null);
                })
            ]
        ];
    }
}
