<?php

namespace Geonodo\Raster\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Geonodo\Raster\Model\LayerRaster
 *
 * @property int $id
 * @property string $description
 * @property int $layer_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Geonodo\Domain\Model\Layer $layer
 * @mixin \Eloquent
 */
class LayerRaster extends Model
{
    protected $table = 'layer_rasters';
    protected $fillable = ['description', 'layer_id'];

    /**
     * Get the layer associated.
     */
    public function layer()
    {
        return $this->belongsTo(Layer::class);
    }
}