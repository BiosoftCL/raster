<?php
namespace Geonodo\Raster\Traits;
use Geonodo\Components\PostgisTable;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Geonodo\Utils\Postgis\Postgis;
use Geonodo\Domain\Model\GeometricType;
use Geonodo\Domain\Model\Layer;
use Geonodo\Raster\Compoments\PostgisRasterTable;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

trait ImportRaster {
    // use ImportLayer;
    protected function importRasterLayer($file, $name, $instance_id, $spatialRefSys= null, $connection = 'default')
    {
        if ( ! file_exists($file)) {
            logger()->error('File not found', ['file' => $file]);
            flash(__('layer.message-error-file-notfound'), 'danger')->important();

            return false;
        }

        $geometricType = GeometricType::find(3);

        /** @var PostgisTable $table */
        $table = new PostgisTable($name);
        $tableName = $table->getTableName();

        $requestBody = array_merge([
            [
                'name'     => 'file',
                'contents' => fopen($file,'r')
            ],
            [
                'name'     => 'tableName',
                'contents' => $tableName
            ]
        ], $this->mapConfDb());

        if($spatialRefSys !== null) {
            array_push($requestBody, [
                'name'     => 'refSys',
                'contents' => $spatialRefSys
            ]);
        }

        try {
            $client = new Client();
            $response = $client->post('http://api-postgis:5007/raster',
            // $response = $client->post('http://192.168.1.6:5007/raster',
            [
                'multipart' => $requestBody
            ]);
        } catch (\Exception $e) {
            Postgis::destroyTable($tableName);
            logger()->error('raster2pgsql import error', [
                'msg'  => $e->getMessage(),
                'code' => $e->getCode()
            ]);
            flash(__('layer.message-error-create-table'), 'danger')->important();
            return false;
        }
        // Determina sistema de referencia
        $spatialRefSys = $this->getLayerRefSys($tableName, false);
        if (empty($spatialRefSys)) {
            return false;
        }

        $layer = Layer::create([
            'instance_id'        => $instance_id,
            'import_type_id'     => 9,
            'name'               => $name,
            'table'              => $tableName,
            'spatial_ref_sys_id' => (int)$spatialRefSys,
            'geometric_type_id'  => $geometricType->id,
            'columns_types'      => json_encode([]),
        ]);

        return $layer;
    }

    /**
     * Determina sistema de referencia de una tabla postgis
     *
     * @param string $tableName
     *
     * @return bool|integer
     */
    protected function getLayerRefSys($tableName, $useMxSigDB)
    {
        try {
            return PostgisRasterTable::spatialRefSys($tableName);
        } catch (QueryException $e) {
            Postgis::destroyTable($tableName);

            flash(__('layer.msg-srdi'), 'danger')->important();
            logger()->error('Postgis get spatial reference system', [
                'msg'  => $e->getMessage(),
                'code' => $e->getCode(),
                'sql'  => $e->getSql()
            ]);

            return false;
        } catch (\Exception $e) {
            Postgis::destroyTable($tableName);
            flash(_($e->getMessage()), 'danger')->important();
            logger()->error('Postgis get spatial reference system', [
                'msg'  => $e->getMessage(),
                'code' => $e->getCode(),
            ]);

            return false;
        }
    }

    private function mapConfDb() {
        $conf_db  = [];
        $confs  = Config::get('database')['connections'][Config::get('database')['default']];

        foreach( $confs as $clave => $valor) {
            array_push($conf_db, [
                'name'     => $clave,
                'contents' => $valor
            ]);
        }
        return $conf_db;


    }

}