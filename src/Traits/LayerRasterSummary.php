<?php

namespace Geonodo\Raster\Traits;

use Geonodo\Components\Ogrinfo;
use Geonodo\Domain\Model\GeometricType;
use Geonodo\Raster\Compoments\Gdalinfo;
use Geonodo\Traits\LayerSummary;
use Illuminate\Support\Facades\File;
use wapmorgan\UnifiedArchive\UnifiedArchive;

/**
 * Trait Geonodo\Raster\Traits\LayerRasterSummary
 *
 * Valida un archivo de capa
 */
trait LayerRasterSummary
{
    use LayerSummary;

    /**
     * @param string $file
     *
     * @return Object
     */
    private function getSummary($file)
    {
        $fileExt = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        // prepara archivo subido si es necesario
        switch ($fileExt) {
            case 'tiff':
                $summary = $this->getGdalinfoSummary($file);
                break;
            // default:
            //     $summary = $this->getOgrinfoSummary($file);
        }

        return $summary;
    }

    /**
     * @param string $layer
     *
     * @return array
     */
    private function getGdalinfoSummary($layer)
    {
        try {
            $summary = Gdalinfo::getLayerInfo($layer);
        } catch (\Exception $e) {
            return [];
        }

        return $summary;
    }

}